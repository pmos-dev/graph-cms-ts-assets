import { commonsTypeHasPropertyT } from 'tscommons-es-core';

import { isTGraphCmsVertexContent, TGraphCmsVertexContent } from '../src/types/tvertex-content';
import { isTGraphCmsEdgeHyperlink, TGraphCmsEdgeHyperlink } from '../src/types/tedge-hyperlink';

export type TGraphCmsContentAndHyperlink<
		C extends TGraphCmsVertexContent = TGraphCmsVertexContent,
		H extends TGraphCmsEdgeHyperlink = TGraphCmsEdgeHyperlink
> = {
		content: C;
		relation: H;
};

export function isTGraphCmsContentAndHyperlink<
		C extends TGraphCmsVertexContent = TGraphCmsVertexContent,
		H extends TGraphCmsEdgeHyperlink = TGraphCmsEdgeHyperlink
>(
		test: unknown,
		isTContent: (_: unknown) => _ is C = (_: unknown): _ is C => isTGraphCmsVertexContent(_),
		isTHyperlink: (_: unknown) => _ is H = (_: unknown): _ is H => isTGraphCmsEdgeHyperlink(_)
): test is TGraphCmsContentAndHyperlink<C, H> {
	if (!commonsTypeHasPropertyT<C>(test, 'content', isTContent)) return false;
	if (!commonsTypeHasPropertyT<H>(test, 'relation', isTHyperlink)) return false;

	return true;
}
