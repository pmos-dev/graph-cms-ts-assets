import { commonsTypeHasPropertyT } from 'tscommons-es-core';

import { isTGraphCmsVertexContent, TGraphCmsVertexContent } from './tvertex-content';
import { isTGraphCmsEdgeSectionContent, TGraphCmsEdgeSectionContent } from './tedge-section-content';

export type TGraphCmsContentAndSectionContent<
		C extends TGraphCmsVertexContent = TGraphCmsVertexContent,
		SC extends TGraphCmsEdgeSectionContent = TGraphCmsEdgeSectionContent
> = {
		content: C;
		relation: SC;
};

export function isTGraphCmsContentAndSectionContent<
		C extends TGraphCmsVertexContent = TGraphCmsVertexContent,
		SC extends TGraphCmsEdgeSectionContent = TGraphCmsEdgeSectionContent
>(
		test: unknown,
		isTContent: (_: unknown) => _ is C = (_: unknown): _ is C => isTGraphCmsVertexContent(_),
		isTSectionContent: (_: unknown) => _ is SC = (_: unknown): _ is SC => isTGraphCmsEdgeSectionContent(_)
): test is TGraphCmsContentAndSectionContent<C, SC> {
	if (!commonsTypeHasPropertyT<C>(test, 'content', isTContent)) return false;
	if (!commonsTypeHasPropertyT<SC>(test, 'relation', isTSectionContent)) return false;

	return true;
}
