import { commonsTypeHasPropertyNumber, commonsTypeHasPropertyT } from 'tscommons-es-core';

import { isTGraphCmsVertexContent, TGraphCmsVertexContent } from './tvertex-content';
import { isTGraphCmsVertexSection, TGraphCmsVertexSection } from './tvertex-section';

export type TGraphCmsRelatedContent<
		C extends TGraphCmsVertexContent = TGraphCmsVertexContent,
		S extends TGraphCmsVertexSection = TGraphCmsVertexSection
> = {
		relatedContent: C;	// NB, the word 'content' doesn't seem to survice the REST transfer for some unknown reason?
		hops: number;
		section: S;
};

export function isTGraphCmsRelatedContent<
		C extends TGraphCmsVertexContent = TGraphCmsVertexContent,
		S extends TGraphCmsVertexSection = TGraphCmsVertexSection
>(
		test: unknown,
		isTContent: (_: unknown) => _ is C = (_: unknown): _ is C => isTGraphCmsVertexContent(_),
		isTSection: (_: unknown) => _ is S = (_: unknown): _ is S => isTGraphCmsVertexSection(_)
): test is TGraphCmsRelatedContent<C, S> {
	if (!commonsTypeHasPropertyT<C>(test, 'relatedContent', isTContent)) return false;
	if (!commonsTypeHasPropertyNumber(test, 'hops')) return false;
	if (!commonsTypeHasPropertyT<S>(test, 'section', isTSection)) return false;

	return true;
}
