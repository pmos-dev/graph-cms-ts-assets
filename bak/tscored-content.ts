import { commonsTypeHasPropertyNumber, commonsTypeHasPropertyT, commonsTypeHasPropertyTArray } from 'tscommons-es-core';

import { TGraphCmsVertexContent, isTGraphCmsVertexContent } from './tvertex-content';
import { TGraphCmsVertexSection, isTGraphCmsVertexSection } from './tvertex-section';

export type TGraphCmsScoredContent<
		C extends TGraphCmsVertexContent = TGraphCmsVertexContent,
		S extends TGraphCmsVertexSection = TGraphCmsVertexSection
> = {
		content: C;
		score: number;
		sections: S[];
};

export function isTGraphCmsScoredContent<
		C extends TGraphCmsVertexContent = TGraphCmsVertexContent,
		S extends TGraphCmsVertexSection = TGraphCmsVertexSection
>(
		test: unknown,
		isTContent: (_: unknown) => _ is C = (_: unknown): _ is C => isTGraphCmsVertexContent(_),
		isTSection: (_: unknown) => _ is S = (_: unknown): _ is S => isTGraphCmsVertexSection(_)
): test is TGraphCmsScoredContent<C, S> {
	if (!commonsTypeHasPropertyT<C>(test, 'content', isTContent)) return false;
	if (!commonsTypeHasPropertyNumber(test, 'score')) return false;
	if (!commonsTypeHasPropertyTArray<S>(test, 'sections', isTSection)) return false;

	return true;
}
