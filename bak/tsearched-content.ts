import { commonsTypeHasPropertyNumber, commonsTypeHasPropertyT, commonsTypeHasPropertyTArray, commonsTypeIsTArray } from 'tscommons-es-core';

import { TGraphCmsVertexContent, isTGraphCmsVertexContent } from './tvertex-content';
import { TGraphCmsVertexSection, isTGraphCmsVertexSection } from './tvertex-section';

export type TGraphCmsSearchedContent<
		C extends TGraphCmsVertexContent = TGraphCmsVertexContent,
		S extends TGraphCmsVertexSection = TGraphCmsVertexSection
> = {
		content: C;
		score: number;
		routes: S[][];
};

type TRoute<S extends TGraphCmsVertexSection> = S[];

function isTRoute<
		S extends TGraphCmsVertexSection = TGraphCmsVertexSection
>(
		test: unknown,
		isTSection: (_: unknown) => _ is S = (_: unknown): _ is S => isTGraphCmsVertexSection(_)
): test is TRoute<S> {
	return commonsTypeIsTArray<S>(test, isTSection);
}

export function isTGraphCmsSearchedContent<
		C extends TGraphCmsVertexContent = TGraphCmsVertexContent,
		S extends TGraphCmsVertexSection = TGraphCmsVertexSection
>(
		test: unknown,
		isTContent: (_: unknown) => _ is C = (_: unknown): _ is C => isTGraphCmsVertexContent(_),
		isTSection: (_: unknown) => _ is S = (_: unknown): _ is S => isTGraphCmsVertexSection(_)
): test is TGraphCmsSearchedContent<C, S> {
	if (!commonsTypeHasPropertyT<C>(test, 'content', isTContent)) return false;
	if (!commonsTypeHasPropertyNumber(test, 'score')) return false;
	if (!commonsTypeHasPropertyTArray<TRoute<S>>(
			test,
			'routes',
			(_: unknown): _ is TRoute<S> => isTRoute(_, isTSection)
	)) return false;

	return true;
}
