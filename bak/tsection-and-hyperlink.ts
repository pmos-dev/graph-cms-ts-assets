import { commonsTypeHasPropertyT } from 'tscommons-es-core';

import { isTGraphCmsVertexSection, TGraphCmsVertexSection } from './tvertex-section';
import { isTGraphCmsEdgeHyperlink, TGraphCmsEdgeHyperlink } from './tedge-hyperlink';

export type TGraphCmsSectionAndHyperlink<
		S extends TGraphCmsVertexSection = TGraphCmsVertexSection,
		H extends TGraphCmsEdgeHyperlink = TGraphCmsEdgeHyperlink
> = {
		section: S;
		relation: H;
};

export function isTGraphCmsSectionAndHyperlink<
		S extends TGraphCmsVertexSection = TGraphCmsVertexSection,
		H extends TGraphCmsEdgeHyperlink = TGraphCmsEdgeHyperlink
>(
		test: unknown,
		isTSection: (_: unknown) => _ is S = (_: unknown): _ is S => isTGraphCmsVertexSection(_),
		isTHyperlink: (_: unknown) => _ is H = (_: unknown): _ is H => isTGraphCmsEdgeHyperlink(_)
): test is TGraphCmsSectionAndHyperlink<S, H> {
	if (!commonsTypeHasPropertyT<S>(test, 'section', isTSection)) return false;
	if (!commonsTypeHasPropertyT<H>(test, 'relation', isTHyperlink)) return false;

	return true;
}
