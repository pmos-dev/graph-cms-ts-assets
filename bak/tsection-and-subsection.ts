import { commonsTypeHasPropertyT } from 'tscommons-es-core';

import { isTGraphCmsVertexSection, TGraphCmsVertexSection } from './tvertex-section';
import { isTGraphCmsEdgeSubsection, TGraphCmsEdgeSubsection } from './tedge-subsection';

export type TGraphCmsSectionAndSubsection<
		S extends TGraphCmsVertexSection = TGraphCmsVertexSection,
		SS extends TGraphCmsEdgeSubsection = TGraphCmsEdgeSubsection
> = {
		section: S;
		relation: SS;
};

export function isTGraphCmsSectionAndSubsection<
		S extends TGraphCmsVertexSection = TGraphCmsVertexSection,
		SS extends TGraphCmsEdgeSubsection = TGraphCmsEdgeSubsection
>(
		test: unknown,
		isTSection: (_: unknown) => _ is S = (_: unknown): _ is S => isTGraphCmsVertexSection(_),
		isTSubsection: (_: unknown) => _ is SS = (_: unknown): _ is SS => isTGraphCmsEdgeSubsection(_)
): test is TGraphCmsSectionAndSubsection<S, SS> {
	if (!commonsTypeHasPropertyT<S>(test, 'section', isTSection)) return false;
	if (!commonsTypeHasPropertyT<SS>(test, 'relation', isTSubsection)) return false;

	return true;
}
