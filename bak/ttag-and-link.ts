import { commonsTypeHasPropertyT } from 'tscommons-es-core';

import { isTGraphCmsVertexTag, TGraphCmsVertexTag } from './tvertex-tag';
import { isTGraphCmsEdgeTagLink, TGraphCmsTagLink } from './tedge-tag-link';

export type TGraphCmsTagAndLink<
		T extends TGraphCmsVertexTag = TGraphCmsVertexTag,
		L extends TGraphCmsTagLink = TGraphCmsTagLink
> = {
		tag: T;
		relation: L;
};

export function isTGraphCmsTagAndLink<
		T extends TGraphCmsVertexTag = TGraphCmsVertexTag,
		L extends TGraphCmsTagLink = TGraphCmsTagLink
>(
		test: unknown,
		isTTag: (_: unknown) => _ is T = (_: unknown): _ is T => isTGraphCmsVertexTag(_),
		isTTagLink: (_: unknown) => _ is L = (_: unknown): _ is L => isTGraphCmsEdgeTagLink(_)
): test is TGraphCmsTagAndLink<T, L> {
	if (!commonsTypeHasPropertyT<T>(test, 'tag', isTTag)) return false;
	if (!commonsTypeHasPropertyT<L>(test, 'relation', isTTagLink)) return false;

	return true;
}
