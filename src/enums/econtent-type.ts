import { commonsTypeIsString } from 'tscommons-es-core';

export enum EGraphCmsContentType {
		ARTICLE = 'article',
		IMAGE = 'image',
		VIDEO = 'video',
		HTML = 'html'
}

export function toEGraphCmsContentType(type: string): EGraphCmsContentType|undefined {
	switch (type) {
		case EGraphCmsContentType.ARTICLE.toString():
			return EGraphCmsContentType.ARTICLE;
		case EGraphCmsContentType.IMAGE.toString():
			return EGraphCmsContentType.IMAGE;
		case EGraphCmsContentType.VIDEO.toString():
			return EGraphCmsContentType.VIDEO;
		case EGraphCmsContentType.HTML.toString():
			return EGraphCmsContentType.HTML;
	}
	return undefined;
}

export function fromEGraphCmsContentType(type: EGraphCmsContentType): string {
	switch (type) {
		case EGraphCmsContentType.ARTICLE:
			return EGraphCmsContentType.ARTICLE.toString();
		case EGraphCmsContentType.IMAGE:
			return EGraphCmsContentType.IMAGE.toString();
		case EGraphCmsContentType.VIDEO:
			return EGraphCmsContentType.VIDEO.toString();
		case EGraphCmsContentType.HTML:
			return EGraphCmsContentType.HTML.toString();
	}
	
	throw new Error('Unknown EContentType');
}

export function isEGraphCmsContentType(test: unknown): test is EGraphCmsContentType {
	if (!commonsTypeIsString(test)) return false;
	
	return toEGraphCmsContentType(test) !== undefined;
}

export function keyToEGraphCmsContentType(key: string): EGraphCmsContentType {
	switch (key) {
		case 'ARTICLE':
			return EGraphCmsContentType.ARTICLE;
		case 'IMAGE':
			return EGraphCmsContentType.IMAGE;
		case 'VIDEO':
			return EGraphCmsContentType.VIDEO;
		case 'HTML':
			return EGraphCmsContentType.HTML;
	}
	
	throw new Error(`Unable to obtain EContentType for key: ${key}`);
}

export const EGRAPHCMS_CONTENT_TYPES: EGraphCmsContentType[] = Object.keys(EGraphCmsContentType)
		.map((e: string): EGraphCmsContentType => keyToEGraphCmsContentType(e));
