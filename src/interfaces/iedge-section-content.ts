import {
		commonsTypeHasPropertyNumberOrUndefined
} from 'tscommons-es-core';

export interface IGraphCmsEdgeSectionContent {
		order?: number;
}

export function isIGraphCmsEdgeSectionContent(test: unknown): test is IGraphCmsEdgeSectionContent {
	if (!commonsTypeHasPropertyNumberOrUndefined(test, 'order')) return false;

	return true;
}
