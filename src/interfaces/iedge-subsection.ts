import {
		commonsTypeHasPropertyNumberOrUndefined
} from 'tscommons-es-core';

export interface IGraphCmsEdgeSubsection {
		order?: number;
}

export function isIGraphCmsEdgeSubsection(test: unknown): test is IGraphCmsEdgeSubsection {
	if (!commonsTypeHasPropertyNumberOrUndefined(test, 'order')) return false;

	return true;
}
