import {
		commonsTypeHasPropertyDate,
		commonsTypeHasPropertyDateOrUndefined,
		commonsTypeHasPropertyEnum,
		commonsTypeHasPropertyNumberOrUndefined,
		commonsTypeHasPropertyString,
		commonsTypeHasPropertyT
} from 'tscommons-es-core';

import { isTGraphCmsUid, TGraphCmsUid } from '../types/tuid';

import { EGraphCmsContentType, isEGraphCmsContentType } from '../enums/econtent-type';

export interface IGraphCmsVertexContent<D extends { [ key: string ]: unknown }> extends TGraphCmsUid {
		contentType: EGraphCmsContentType;
		title: string;

		data: D;

		created: Date;
		
		selectedTally?: number;
		lastSelected?: Date;
}

export function isIGraphCmsVertexContent<D extends { [ key: string ]: unknown }>(
		test: unknown,
		isD: (d: unknown) => d is D
): test is IGraphCmsVertexContent<D> {
	if (!isTGraphCmsUid(test)) return false;

	if (!commonsTypeHasPropertyEnum<EGraphCmsContentType>(test, 'contentType', isEGraphCmsContentType)) return false;

	if (!commonsTypeHasPropertyString(test, 'title')) return false;
	
	if (!commonsTypeHasPropertyT<D>(test, 'data', isD)) return false;

	if (!commonsTypeHasPropertyDate(test, 'created')) return false;
	if (!commonsTypeHasPropertyNumberOrUndefined(test, 'selectedTally')) return false;
	if (!commonsTypeHasPropertyDateOrUndefined(test, 'lastSelected')) return false;

	return true;
}
