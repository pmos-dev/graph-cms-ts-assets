import {
		commonsTypeHasPropertyDate,
		commonsTypeHasPropertyString
} from 'tscommons-es-core';

import { isTGraphCmsUid, TGraphCmsUid } from '../types/tuid';

export interface IGraphCmsVertexSection extends TGraphCmsUid {
		title: string;

		created: Date;
}

export function isIGraphCmsVertexSection(test: unknown): test is IGraphCmsVertexSection {
	if (!isTGraphCmsUid(test)) return false;

	if (!commonsTypeHasPropertyString(test, 'title')) return false;

	if (!commonsTypeHasPropertyDate(test, 'created')) return false;

	return true;
}
