import {
		commonsTypeHasPropertyNumberOrUndefined,
		commonsTypeHasPropertyString,
		commonsTypeHasPropertyStringArrayOrUndefined
} from 'tscommons-es-core';

export interface IGraphCmsVertexTag {
		name: string;
		aliases?: string[];
		weight?: number;
}

export function isIGraphCmsVertexTag(test: unknown): test is IGraphCmsVertexTag {
	if (!commonsTypeHasPropertyString(test, 'name')) return false;
	if (!commonsTypeHasPropertyStringArrayOrUndefined(test, 'aliases')) return false;
	if (!commonsTypeHasPropertyNumberOrUndefined(test, 'weight')) return false;

	return true;
}
