import { commonsTypeIsObject } from 'tscommons-es-core';

import { IGraphCmsEdgeHyperlink } from '../interfaces/iedge-hyperlink';

export type TGraphCmsEdgeHyperlink = Readonly<IGraphCmsEdgeHyperlink>;

export function isTGraphCmsEdgeHyperlink(test: unknown): test is TGraphCmsEdgeHyperlink {
	if (!commonsTypeIsObject(test)) return false;

	return true;
}

export function encodeIGraphCmsEdgeHyperlink(_subsection: IGraphCmsEdgeHyperlink): TGraphCmsEdgeHyperlink {
	return {};
}

export function decodeIGraphCmsEdgeHyperlink(_encoded: TGraphCmsEdgeHyperlink): IGraphCmsEdgeHyperlink {
	return {};
}
