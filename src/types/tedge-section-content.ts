import {
		commonsTypeHasPropertyNumberOrUndefined
} from 'tscommons-es-core';

import { IGraphCmsEdgeSectionContent } from '../interfaces/iedge-section-content';

export type TGraphCmsEdgeSectionContent = Omit<IGraphCmsEdgeSectionContent, 'order'>;

type TSectionContentWithOrder = TGraphCmsEdgeSectionContent & { order: number };

export function isTGraphCmsEdgeSectionContent(test: unknown): test is TGraphCmsEdgeSectionContent {
	if (!commonsTypeHasPropertyNumberOrUndefined(test, 'order')) return false;

	return true;
}

export function encodeIGraphCmsEdgeSectionContent(sectionContent: IGraphCmsEdgeSectionContent): TGraphCmsEdgeSectionContent {
	const encoded: TGraphCmsEdgeSectionContent = {};

	if (sectionContent.order !== undefined) (encoded as TSectionContentWithOrder).order = sectionContent.order;

	return encoded;
}

export function decodeIGraphCmsEdgeSectionContent(encoded: TGraphCmsEdgeSectionContent): IGraphCmsEdgeSectionContent {
	const decoded: IGraphCmsEdgeSectionContent = {};

	if ((encoded as TSectionContentWithOrder).order !== undefined) decoded.order = (encoded as TSectionContentWithOrder).order;

	return decoded;
}
