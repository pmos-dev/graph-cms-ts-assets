import {
		commonsTypeHasPropertyNumberOrUndefined
} from 'tscommons-es-core';

import { IGraphCmsEdgeSubsection } from '../interfaces/iedge-subsection';

export type TGraphCmsEdgeSubsection = Omit<IGraphCmsEdgeSubsection, 'order'>;

type TSubsectionWithOrder = TGraphCmsEdgeSubsection & { order: number };

export function isTGraphCmsEdgeSubsection(test: unknown): test is TGraphCmsEdgeSubsection {
	if (!commonsTypeHasPropertyNumberOrUndefined(test, 'order')) return false;

	return true;
}

export function encodeIGraphCmsEdgeSubsection(subsection: IGraphCmsEdgeSubsection): TGraphCmsEdgeSubsection {
	const encoded: TGraphCmsEdgeSubsection = {};

	if (subsection.order !== undefined) (encoded as TSubsectionWithOrder).order = subsection.order;

	return encoded;
}

export function decodeIGraphCmsEdgeSubsection(encoded: TGraphCmsEdgeSubsection): IGraphCmsEdgeSubsection {
	const decoded: IGraphCmsEdgeSubsection = {};

	if ((encoded as TSubsectionWithOrder).order !== undefined) decoded.order = (encoded as TSubsectionWithOrder).order;

	return decoded;
}
