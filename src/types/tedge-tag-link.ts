import { commonsTypeIsObject } from 'tscommons-es-core';

import { IGraphCmsEdgeTagLink } from '../interfaces/iedge-tag-link';

export type TGraphCmsEdgeTagLink = Readonly<IGraphCmsEdgeTagLink>;

export function isTGraphCmsEdgeTagLink(test: unknown): test is TGraphCmsEdgeTagLink {
	if (!commonsTypeIsObject(test)) return false;

	return true;
}

export function encodeIGraphCmsEdgeTagLink(_subsection: IGraphCmsEdgeTagLink): TGraphCmsEdgeTagLink {
	return {};
}

export function decodeIGraphCmsEdgeTagLink(_encoded: TGraphCmsEdgeTagLink): IGraphCmsEdgeTagLink {
	return {};
}
