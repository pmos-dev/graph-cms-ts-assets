import { commonsBase62HasPropertyId } from 'tscommons-es-core';

export type TGraphCmsUid = {
		uid: string;
};

export function isTGraphCmsUid(test: unknown): test is TGraphCmsUid {
	if (!commonsBase62HasPropertyId(test, 'uid')) return false;

	return true;
}
