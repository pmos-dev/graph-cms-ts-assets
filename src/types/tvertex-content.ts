import {
		commonsDateDateToYmdHis,
		commonsDateYmdHisToDate,
		commonsTypeDecodePropertyObject,
		commonsTypeEncodePropertyObject,
		commonsTypeHasPropertyEnum,
		commonsTypeHasPropertyNumberOrUndefined,
		commonsTypeHasPropertyObject,
		commonsTypeHasPropertyString,
		commonsTypeHasPropertyStringOrUndefined,
		commonsTypeIsEncodedObject,
		COMMONS_REGEX_PATTERN_DATETIME_YMDHIS,
		TEncodedObject
} from 'tscommons-es-core';

import { IGraphCmsVertexContent } from '../interfaces/ivertex-content';

import { EGraphCmsContentType, isEGraphCmsContentType } from '../enums/econtent-type';

import { isTGraphCmsUid } from './tuid';

export type TGraphCmsVertexContent = Omit<
		IGraphCmsVertexContent<{ [ key: string ]: unknown }>,
		'data' | 'created' | 'selectedTally' | 'lastSelected'
> & {
		data: TEncodedObject;
		created: string;
};

type TWithTouchTally = TGraphCmsVertexContent & { selectedTally: number };
type TWithLastTouched = TGraphCmsVertexContent & { lastSelected: string };

export function isTGraphCmsVertexContent(test: unknown): test is TGraphCmsVertexContent {
	if (!isTGraphCmsUid(test)) return false;

	if (!commonsTypeHasPropertyEnum<EGraphCmsContentType>(test, 'contentType', isEGraphCmsContentType)) return false;

	if (!commonsTypeHasPropertyString(test, 'title')) return false;
	if (!commonsTypeHasPropertyStringOrUndefined(test, 'description')) return false;
	if (!commonsTypeHasPropertyStringOrUndefined(test, 'thumbnail')) return false;
	
	if (!commonsTypeHasPropertyObject(test, 'data')) return false;
	if (!commonsTypeIsEncodedObject(test['data'])) return false;

	if (!commonsTypeHasPropertyString(test, 'created')) return false;
	if (!COMMONS_REGEX_PATTERN_DATETIME_YMDHIS.test(test['created'] as string)) return false;

	if (!commonsTypeHasPropertyNumberOrUndefined(test, 'selectedTally')) return false;

	if (!commonsTypeHasPropertyStringOrUndefined(test, 'lastSelected')) return false;
	if (test['lastSelected'] && !COMMONS_REGEX_PATTERN_DATETIME_YMDHIS.test(test['lastSelected'] as string)) return false;

	return true;
}

export function encodeIGraphCmsVertexContent(content: IGraphCmsVertexContent<{ [ key: string ]: unknown }>): TGraphCmsVertexContent {
	const encoded: TGraphCmsVertexContent = {
			uid: content.uid,
			contentType: content.contentType,
			title: content.title,
			data: commonsTypeEncodePropertyObject(content.data),
			created: commonsDateDateToYmdHis(content.created)
	};

	if (content.lastSelected !== undefined) (encoded as TWithLastTouched).lastSelected = commonsDateDateToYmdHis(content.lastSelected);
	if (content.selectedTally !== undefined) (encoded as TWithTouchTally).selectedTally = content.selectedTally;

	return encoded;
}

export function decodeIGraphCmsVertexContent(encoded: TGraphCmsVertexContent): IGraphCmsVertexContent<{ [ key: string ]: unknown }> {
	const decoded: IGraphCmsVertexContent<{ [ key: string ]: unknown }> = {
			uid: encoded.uid,
			contentType: encoded.contentType,
			title: encoded.title,
			data: commonsTypeDecodePropertyObject(encoded.data),
			created: commonsDateYmdHisToDate(encoded.created)
	};

	if ((encoded as TWithLastTouched).lastSelected !== undefined) decoded.lastSelected = commonsDateYmdHisToDate((encoded as TWithLastTouched).lastSelected);
	if ((encoded as TWithTouchTally).selectedTally !== undefined) decoded.selectedTally = (encoded as TWithTouchTally).selectedTally;

	return decoded;
}
