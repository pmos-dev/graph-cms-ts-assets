import {
		commonsDateDateToYmdHis,
		commonsDateYmdHisToDate,
		commonsTypeHasPropertyString,
		COMMONS_REGEX_PATTERN_DATETIME_YMDHIS
} from 'tscommons-es-core';

import { IGraphCmsVertexSection } from '../interfaces/ivertex-section';

import { isTGraphCmsUid } from './tuid';

export type TGraphCmsVertexSection = Omit<IGraphCmsVertexSection, 'created'> & {
		created: string;
};

export function isTGraphCmsVertexSection(test: unknown): test is TGraphCmsVertexSection {
	if (!isTGraphCmsUid(test)) return false;

	if (!commonsTypeHasPropertyString(test, 'title')) return false;

	if (!commonsTypeHasPropertyString(test, 'created')) return false;
	if (!COMMONS_REGEX_PATTERN_DATETIME_YMDHIS.test(test['created'] as string)) return false;

	return true;
}

export function encodeIGraphCmsVertexSection(section: IGraphCmsVertexSection): TGraphCmsVertexSection {
	const encoded: TGraphCmsVertexSection = {
			uid: section.uid,
			title: section.title,
			created: commonsDateDateToYmdHis(section.created)
	};

	return encoded;
}

export function decodeIGraphCmsVertexSection(encoded: TGraphCmsVertexSection): IGraphCmsVertexSection {
	const decoded: IGraphCmsVertexSection = {
			uid: encoded.uid,
			title: encoded.title,
			created: commonsDateYmdHisToDate(encoded.created)
	};

	return decoded;
}
