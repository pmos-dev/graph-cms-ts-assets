import {
		commonsTypeHasPropertyNumberOrUndefined,
		commonsTypeHasPropertyString,
		commonsTypeHasPropertyStringArrayOrUndefined
} from 'tscommons-es-core';

import { IGraphCmsVertexTag } from '../interfaces/ivertex-tag';

export type TGraphCmsVertexTag = Omit<IGraphCmsVertexTag, 'aliases' | 'weight'>;

type TWithAliases = TGraphCmsVertexTag & { aliases: string[] };
type TWithWeight = TGraphCmsVertexTag & { weight: number };

export function isTGraphCmsVertexTag(test: unknown): test is TGraphCmsVertexTag {
	if (!commonsTypeHasPropertyString(test, 'name')) return false;
	if (!commonsTypeHasPropertyStringArrayOrUndefined(test, 'aliases')) return false;
	if (!commonsTypeHasPropertyNumberOrUndefined(test, 'weight')) return false;

	return true;
}

export function encodeIGraphCmsVertexTag(tag: IGraphCmsVertexTag): TGraphCmsVertexTag {
	const encoded: TGraphCmsVertexTag = {
			name: tag.name
	};

	if (tag.aliases !== undefined) (encoded as TWithAliases).aliases = tag.aliases;
	if (tag.weight !== undefined) (encoded as TWithWeight).weight = tag.weight;

	return encoded;
}

export function decodeIGraphCmsVertexTag(encoded: TGraphCmsVertexTag): IGraphCmsVertexTag {
	const decoded: IGraphCmsVertexTag = {
			name: encoded.name
	};

	if ((encoded as TWithAliases).aliases !== undefined) decoded.aliases = (encoded as TWithAliases).aliases;
	if ((encoded as TWithWeight).weight !== undefined) decoded.weight = (encoded as TWithWeight).weight;

	return decoded;
}
